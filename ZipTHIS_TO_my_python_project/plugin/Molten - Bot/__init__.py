#!/usr/bin/python
# -*- coding: UTF-8 -*-
__author__ = 'maxisoft'

import urllib

try:
	from plugin.BotVoteBase import *
except:
	from BotVoteBase import *


class MoltenRefererHeader(MyRequest):
	def __init__(self, url, data=None, headers=MyRequest.request_headers, origin_req_host=None, unverifiable=False,
	             referer=None):
		if referer:
			headers["Referer"] = str(referer)
		MyRequest.__init__(self, url, data, headers, origin_req_host, unverifiable)


class MoltenBot(BotBase):
	pluginprior = 0  # No prior
	sitename = u"Molten"  # Short name (db records)
	votewaittime = 12 * 60  # wait time between vote (in min)

	def vote_available(self, topid):
		assert self.pagevote
		return u"value='{}'".format(str(topid)) in self.pagevote

	def login(self, account, **kwargs):
		values = {"rememberme": "1", "strID": account.login, "strPW": account.decrypt_pass()}
		data = urllib.urlencode(values)  # create string
		request = MyRequest("https://www.molten-wow.com/login/", data)  # request object
		url = self.urlo.open(request)  # send request
		content = unicode(url.read(5000), 'utf8')
		return content.find(u'Logged in as {}'.format(account.login)) >= 0  # found ok string

	def vote(self, account, **kwargs):
		votedtop = []  # list sucess
		self.pagevoteurl = self.urlo.open(
			MoltenRefererHeader("https://www.molten-wow.com/account/pg/0:1:0:v:undefined/",
			                    referer="https://www.molten-wow.com/account/"))
		self.pagevote = unicode(self.pagevoteurl.read(), 'utf8')

		tops = ["Open Wow", "XtremeTop100", "ArenaTop100"]  # all tops
		referers = ["http://www.openwow.com/visit=6",
		            "http://www.xtremetop100.com/out.php?site=1132296123",
		            "http://wow.top100arena.com/out.asp?id=44178"]
		for (i, top), referer in zip(enumerate(tops), referers):
			values = {"id": str(i + 1)}
			if not self.vote_available(i + 1):  # cant vote on this top
				continue
			#else
			data = urllib.urlencode(values)  # create string
			request = MyRequest("https://www.molten-wow.com/account/vote/", data)  # request object
			url = self.urlo.open(request)  # send request
			url.read(1)

			# callback molten-wow website with top's referer
			url = self.urlo.open(MoltenRefererHeader("http://www.molten-wow.com", referer=referer))
			url.read()

			votedtop.append(top)

		# clean current object
		del self.pagevoteurl
		del self.pagevote

		return votedtop