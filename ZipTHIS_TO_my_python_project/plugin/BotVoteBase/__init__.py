#!/usr/bin/python
# -*- coding: UTF-8 -*-

from threading import Lock
import urllib2
import time
import datetime
import re
import random
import gc

from plugin import *
try:
	import cPickle as pickle
except ImportError:
	import pickle

from DB import Vote, Account, create_database, BotVoteStore
from storm.expr import Select, Not, Or, And

class MyRequest(urllib2.Request):
	"""
	urllib2.Request with other header default value.
	"""
	request_headers = {
	"User-Agent": r"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31",
	"Accept-Language": r"fr,fr-fr;q=0.8,en-us;q=0.5,en;q=0.3",
	"Accept": r"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
	"Connection": r"keep-alive",
	"Accept-Charset": "ISO-8859-1,utf-8;q=0.7,*;q=0.3"
	}

	def __init__(self, url, data=None, headers=request_headers, origin_req_host=None, unverifiable=False):
		urllib2.Request.__init__(self, url, data, headers, origin_req_host, unverifiable)


class StringCookieJar(cookielib.CookieJar):
	def __init__(self, string=None, policy=None):
		cookielib.CookieJar.__init__(self, policy)
		if string:
			self._cookies = pickle.loads(string)

	def dump(self):
		return pickle.dumps(self._cookies)


class BotUrlOpener(object):
	def __init__(self, proxy=None, stringcookie=None):
		self.state = "build"
		self.handler = []
		self.cookiejar = cookielib.CookieJar()
		self.add_handler(urllib2.HTTPCookieProcessor(self.cookiejar))
		self.proxy = proxy
		if self.proxy:
			if not isinstance(self.proxy, urllib2.ProxyHandler):
				self.proxy = urllib2.ProxyHandler(proxy)
			self.add_handler(self.proxy)
		self.__urlo = urllib2.build_opener(*self.handler)
		self.open = self.__urlo.open
		self.state = "run"

	def clear_cookies(self):
		self.cookiejar.clear()

	def add_handler(self, h):
		assert self.state == "build"
		self.handler.append(h)

	def get_ip(self):
		data = str(self.open('http://checkip.dyndns.com/').read())
		return re.compile(r'Address: (\d+\.\d+\.\d+\.\d+)').search(data).group(1)



class BotBase(RequieredPlugin):

	sitename = ""  # name for DB record
	changeip = False  # TODO
	votewaittime = 50  # in min
	waitthread = False  # do not change
	_lock = Lock()

	def __init__(self, name=None, tiemoutjoin=100):
		super(BotBase, self).__init__(name, tiemoutjoin)
		self.urlo = BotUrlOpener()
		self.__db = None
		self.store = None
		self._continue = True

	@abstractmethod
	def login(self, account, **kwargs):
		"""
		defaut method to login.
		Must return bool True if login sucess, False else.
		login credential are in account obj : login, passw = account.login, decrypt(account.password)
		"""
		pass

	@abstractmethod
	def vote(self, account, **kwargs):
		"""
		Defaut vote method.
		Must return "top vote site" list in order to identify successfull or fail.
		"""
		pass

	def doAll(self):
		self.__db = create_database('sqlite:%s' % os.path.join(INSTALL_DIR, 'DB.db'))
		self.store = BotVoteStore(self.__db)
		accs = self.store.get_available_account(self.sitename, self.votewaittime)
		self.logger.debug("get_available_account result : %s", [acc.login for acc in accs])
		for acc in accs:
			proxy = None
			if acc.proxy:
				proxy = {'http': acc.proxy, 'https': acc.proxy}
			self.urlo = BotUrlOpener(proxy=proxy)
			try:
				sucess = self.login(acc)
				if not sucess:
					self.logger.warn('login error with account : %s', acc.login)
					continue
			except Exception, e:
				self.logger.exception('during login in with account : %s', acc.login)
				continue

			try:
				toplist = self.vote(acc)
				if toplist:
					self.logger.info('Vote sucessfull with account : %s; on tops : %s', acc.login, str(toplist))
					for top in toplist:
						vote = Vote(top, acc, sitename=acc.sitename)
						try:
							vote.ip = acc.proxy or unicode(self.urlo.get_ip())
						except:
							vote.ip = "UNKNOWN"
							self.logger.warn("Can't get Ip Address.")
						self.store.register_vote(vote)
			except Exception, e:
				self.logger.exception('during vote with account : %s', acc.login)
				continue
			finally:
				time.sleep(0.1)

		self.store.close()
		del self.store
		del self.__db
		gc.collect()

	def run(self):
		#self.logger.debug('Using DB : sqlite:%s' % os.path.join(INSTALL_DIR, 'DB.db'))
		while self._continue:
			try:
				self._lock.acquire()
				self.doAll()
			except ImportError, e:
				if "because the import lock" in e:
					self.logger.debug("import is locked")
				else:
					self.logger.exception('')
			except:
				self.logger.exception('')
			finally:
				self._lock.release()
			time.sleep(60+random.randint(1, 40))



