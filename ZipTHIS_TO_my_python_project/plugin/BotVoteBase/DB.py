from storm.locals import *
import datetime
import json
from utils.utils import decrypt, read_iv
from theglobals import KEY_PRIVATE_FILE

__doc__ = """Classes representing DB records"""

class Account(Storm):
	__storm_table__ = "vote_account"
	id = Int(primary=True)
	login = Unicode()
	password = Unicode()
	sitename = Unicode()
	priority = Int()
	proxy = Unicode()

	def __storm_flushed__(self):
		self.id = AutoReload
		self.priority = AutoReload

	def decrypt_pass(self):
		return decrypt(self.password, read_iv(KEY_PRIVATE_FILE))

	def to_json(self):
		return dict(id=self.id, login=self.login, sitename=self.sitename, priority=self.priority, proxy=self.proxy)

class Vote(Storm):
	__storm_table__ = "vote_vote"
	id = Int(primary=True)
	sitename = Unicode()
	account_id = Int()
	account = Reference(account_id, Account.id)
	date = DateTime()
	top = Unicode()
	ip = Unicode()

	def __init__(self, top, account=None, date=None, sitename=None):
		date = date or datetime.datetime.now()
		self.top = unicode(top)
		if account is not None:
			self.set_account(account)
		if sitename is not None:
			self.sitename = unicode(sitename)
		self.date = date

	def set_account(self, account):
		if isinstance(account, (int, long, Int)):
			self.account_id = account
		elif isinstance(account, Account):
			self.account_id = account.id
			self.account = account
		else:
			raise TypeError('account type')
		self.sitename = self.account.sitename

	def __storm_flushed__(self):
		self.id = AutoReload

	def to_json(self):
		return dict(id=self.id, sitename=self.sitename, account=self.account, date=self.date.isoformat(), top=self.top, ip=self.ip)


class BotVoteStore(Store):
	"""
	Storm store with my utils methods.
	"""
	def __init__(self, database, cache=None):
		super(BotVoteStore, self).__init__(database, cache)

	def register_vote(self, vote):
		assert isinstance(vote, Vote)
		self.add(vote)
		self.commit()
		self.flush()

	def add_account(self, account):
		assert isinstance(account, Account)
		self.add(account)
		self.commit()
		self.flush()

	def is_vote_available(self, checkvote, wait_time=20):
		assert isinstance(checkvote, Vote)
		res = self.find(Vote, Vote.account_id == checkvote.account_id, Vote.top == checkvote.top,
							Vote.sitename == checkvote.sitename,
							not Vote.date > checkvote.date - datetime.timedelta(minutes=wait_time))
		return bool(res.count())

	def get_available_account(self, site, waittime=120):
		subselect = Select(Account.id, And(Account.id == Vote.account_id, Vote.sitename == unicode(site)), distinct=True)
		new_accs = self.find(Account , And(Not(Account.id.is_in(subselect)), Account.sitename == unicode(site)))
		ret = self.find((Account, Max(Vote.date)),
			And(Account.sitename == unicode(site), Account.id == Vote.account_id,
					Vote.date <= datetime.datetime.now()))\
			.group_by(Account.id)\
			.order_by(Account.priority)
		return [acc for acc, date in ret if
		        not date or datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S.%f") + datetime.timedelta(minutes=waittime)
		        <= datetime.datetime.now()] + [new_acc for new_acc in new_accs]

	def get_all_accounts(self, site):
		return self.find(Account, Account.sitename == unicode(site)).order_by(Account.priority)

	def get_all_vote(self, site):
		return self.find(Vote, Vote.sitename == unicode(site)).order_by(Vote.date)

	def delete_account(self,account):
		id = None
		if isinstance(account, (int, long, Int)):
			id = account
		elif isinstance(account, Account):
			id = account.id
		else:
			raise TypeError('account type')
		self.execute('DELETE FROM "vote_account" WHERE ("id"=%s)' % str(id))
		self.commit()
		self.flush()


class ComplexEncoder(json.JSONEncoder):
	def default(self, obj):
		if hasattr(obj, 'to_json'):
			return obj.to_json()
		else:
			return json.JSONEncoder.default(self, obj)