#!/usr/bin/python
# -*- coding: UTF-8 -*-
__author__ = 'maxisoft'

import urllib

try:
	from plugin.BotVoteBase import *
except:
	from BotVoteBase import *



class EarthquakeBot(BotBase):
	pluginprior = 0  # No prior
	sitename = u"Earthquake"  # Short name (db records)
	votewaittime = 2 * 60  # wait time between vote (in min)
	spoted = False


	def updateSession(self):
		request = MyRequest("http://earthquake-servers.com/updateSession.php")
		url = self.urlo.open(request)
		return url.read(5000)

	def vote_available(self):
		return 'http://www.rpg-paradize.com/?page=vote&vote=34922' in self.pagevote

	def login(self, account, **kwargs):
		"""Connexion au site http://earthquake-servers.com .
		Retourne True si connexion ok."""
		if self.spoted:
			return []
		values = {'connexion_username': account.login, 'connexion_password': account.decrypt_pass(), 'connexion_active':'yes'}
		data = urllib.urlencode(values)
		request = MyRequest("http://earthquake-servers.com/index.php", data)
		url = self.urlo.open(request)
		ret = url.read(5000000).find(str(account.login)) != -1
		return ret and u"connected" in unicode(self.updateSession(), 'utf-8')


	def getVoteVerif(self):
		"""Recupere le 'voteVerif' constituer d'hex."""
		m = re.findall('(?<=&voteVerif=)[0-9a-fA-F]*(?!c=)', self.pagevote)
		if not m:
			return False
		return m[0].strip()

	def decoupe(self):
		tmp = self.pagevote.split('style type="text/css"')
		assert len(tmp), "impossible de voter, la page vote a peut être été mise a jour."
		self.pagevote = tmp[1]

	def getHex(self):
		"""recherche le doublon hex"""
		pat = re.compile(r'\s+') #supprime tous les chars echapés
		hexs = [pat.sub('', x).replace('{display:inline;}', '')[1:]
			 for x in re.findall(r"\.+.+\n+\{display : inline;}", self.pagevote)]
		for hex in hexs:
			if self.pagevote.count(hex) > 1:  # => yen a au moin deux
				return hex

		#si pas trouve
		return False

	def getcss(self):
		"""Retourne le css (necessite le hex)"""
		abc = re.findall(r""".{27}class="%s\"""" % self.hex, self.pagevote)
		assert len(abc) == 1, "impossible de voter, la page vote a peut être été mise a jour."
		return abc[0].split(',')[2].strip()

	def vote(self, account, **kwargs):
		if self.spoted:
			return []
		request = MyRequest('http://earthquake-servers.com/newvoter.php')
		url = self.urlo.open(request)
		pageVote = url.read(500000)
		self.pagevote = pageVote
		if not self.vote_available():
			return []
		self.voteverif = self.getVoteVerif()
		if not self.voteverif:
			return []
		self.decoupe()

		self.hex = self.getHex()
		assert self.hex, "impossible de voter, la page vote a peut être été mise a jour."
		self.css = self.getcss()


		request = MyRequest('http://earthquake-servers.com/newvoter.php' +
		                          """?voteID=%s&voteVerif=%s&c=temp&css=%s""" % (3, self.voteverif, self.css))
		url = self.urlo.open(request)
		ret = []
		content = url.read(500000)
		if content.find("ERROR_BOT") > -1:
			self.spoted = True
			raise Exception("Bot Detectée !")

		if content.find('OK_VOTE') > -1:
			ret = ["RPG-Paradize"]
		self.updateSession()


		#clean
		del self.hex
		del self.css
		del self.pagevote

		return ret