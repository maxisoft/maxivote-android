#!/usr/bin/python
# -*- coding: UTF-8 -*-
__author__ = 'maxisoft'

from plugin import *
import urllib2
import urllib
try:
	from plugin.BotVoteBase import *
except:
	from BotVoteBase import *


class AvaBot(BotBase):
	pluginprior = 0  # aucune priorité
	sitename = "Ad Vitam Aeternam"  # Nom du site (sert pour les enregistrement DB)
	votewaittime = 122  # permet de verifier toute les X minutes si on peut voter. Precision de plus ou moins 2 minutes

	def login(self, account, **kwargs):
		"""
		Comme on n'a pas besoin de mot de passe pour voter ici,
		on dit que le login est toujour ok.
		"""
		return True

	def vote(self, account, **kwargs):
		"""
		Lance le vote sur RPG paradise.
		Ne Vote pas sur Gowonda (ne peut pas resoudre le captcha)
		"""
		# On crée un dict contenant les valeurs a envoyer en post pour voter.
		values = {'step': '2', 'account': account.login}  # voir http://www.aeternam-serveur.fr/js/portal.core.js
		data = urllib.urlencode(values)  # crée un string "encodant" les valeurs du dict
		request = MyRequest("http://www.aeternam-serveur.fr/vote", data)  # crée un object request
		# on lance la requete
		url = self.urlo.open(request, timeout=100)
		# lire la reponse
		page = url.read(500)
		#self.logger.debug("-------------\nread : \n%s\n---------------", page)

		# Verification que l'on peut voter sur "RPG-Paradize"
		if not "RPG-Paradize" in page or not "onclick=\"launchVote(1" in page:
			return []  # impossible de voter

		#sinon on lance le vote
		values = {'action': 'setvote', 'root_id': "1"}
		data = urllib.urlencode(values)
		request = MyRequest("http://www.aeternam-serveur.fr/vote", data)
		url = self.urlo.open(request, timeout=100)
		url.read(1)
		return ["RPG-Paradize"]