var botvote = {};

botvote.pluginlist = null;
botvote.tips = $(".validateTips");
botvote.tabs = $("div#tabs");
botvote.data = [];


botvote.getPluginList = function () {
    if (this.pluginlist) {
        return this.pluginlist
    }
    $.ajax({
        type: "GET",
        url: "get_all_vote_plugins",
        success: function (msg) {
            botvote.pluginlist = msg
            botvote.updatePage()
        }});
}

botvote.deleteAccount = function (id) {
    $.ajax({
        type: "GET",
        url: "delete_account",
        data: {
            id: id
        },
        success: function (msg) {
            if (msg["err"]) {
                //TODO noty err
                return;
            }
            botvote.getAccounts();
        }
    });
}

botvote.modAccount = function(id){
    alert("TODO");
}

botvote.updateHTMLWithData = function (site, div) {
    $(div).html(this.data["html"]);
    $.each(this.data[site], function (index, value) {
        $("#users tbody", div).append("<tr id=acc_" + value["id"] + ">" +
            "<td>" + value.login + "</td>" +
            "<td>" + (value.proxy || "None") + "</td>" +
            "<td>" + value.priority + "</td>" +
            "<td style='text-align: center'>" + "<button class='del_acc' onclick='botvote.deleteAccount(" + value["id"] + ")'>Delete</button>" +
            "<button class='mod_acc' onclick='botvote.modAccount(" + value["id"] + ")'>Change</button>" + "</td>" +
            "</tr>");
    });
    botvote.initDialog(div);
}

botvote.getAccounts = function (site, div) {
    site = site || this.activeTabText();
    div = div || this.activeTabDiv();
    $.ajax({
        type: "GET",
        url: "get_all_account",
        data: {site: site},
        success: function (msg) {
            botvote.data[site] = msg;
            botvote.updateHTMLWithData(site, div)
        }
    });
}

/**
 * Select li active node
 * @returns jQuery
 */
botvote.activeTab = function () {
    var index = this.tabs.tabs("option", "active");
    return $("ul>li", this.tabs)[index]
}

/**
 *
 * @returns string
 */
botvote.activeTabText = function (li) {
    var jq = li || this.activeTab();
    return $("a", jq).text()
}

botvote.activeTabDiv = function () {
    var jq = this.activeTab();
    var href = $("a", jq).attr('href')
    return $("div" + href)
}

botvote.updatePage = function () {
    $.each(this.pluginlist, function (index, value) {
        var esc_value = value.replace(/ /g, '_')
        $("div#tabs>ul").append("<li><a href='#" + esc_value + "'>" + value + "</a></li>");
        $("div#tabs").append("<div id=\"" + esc_value +
            "\">\n\t" +
            botvote.data["html"]
            +
            "</div>");
        botvote.initDialog($("div#" + esc_value))
    });


    $(".spinner").spinner();
    var tabs = botvote.tabs.tabs({
        beforeActivate: function (event, ui) {
            ui.oldPanel.html("");
            botvote.getAccounts(botvote.activeTabText(ui.newTab), ui.newPanel);
        }}).addClass("ui-tabs-vertical ui-helper-clearfix");
    $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
    botvote.getAccounts()
}

botvote.updateTips = function (t) {
    this.tips
        .text(t)
        .addClass("ui-state-highlight");
    setTimeout(function () {
        botvote.tips.removeClass("ui-state-highlight", 1500);
    }, 500);
}

botvote.checkLength = function (o, n, min, max) {
    if (o.val().length > max || o.val().length < min) {
        o.addClass("ui-state-error");
        botvote.updateTips("Length of " + n + " must be longer than " +
            (min - 1) + ".");
        return false;
    } else {
        return true;
    }
}


botvote.initDialog = function (sub) {

    var spinner = $(".spinner").spinner();

    var name = $("#name"),
        password = $("#password"),
        proxy = $("#proxy"),
        allFields = $([]).add(name).add(password);

    $("#dialog-form").dialog({
        autoOpen: false,
        height: 440,
        width: 350,
        modal: true,
        buttons: {
            Add: function () {
                var activetabname = botvote.activeTabText();
                var bValid = true;
                allFields.removeClass("ui-state-error");
                bValid = bValid && botvote.checkLength(name, "username", 2, 500);
                //bValid = bValid && botvote.checkLength(password, "password", 1, 500);
                if (bValid) {
                    var dlg = $(this)
                    $.ajax({
                        type: "GET",
                        url: "add_account",
                        data: {
                            sitename: botvote.activeTabText(),
                            login: name.val(),
                            password: password.val(),
                            proxy: proxy.val(),
                            prior: spinner.val()
                        },
                        success: function (msg) {
                            if (msg["err"]) {
                                botvote.updateTips(msg["err"]);
                                return;
                            }
                            botvote.getAccounts();
                            dlg.dialog("close");
                        }
                    });
                    /*$("#users tbody", botvote.activeTabDiv()).append("<tr>" +
                     "<td>" + name.val() + "</td>" +
                     "<td>" + proxy.val() + "</td>" +
                     "<td>" + proxy.val() + "</td>" +
                     "</tr>");*/

                }
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        },
        show: {
            effect: "drop",
            duration: 400
        },
        hide: {
            effect: "drop",
            duration: 300
        }
    });
    $("#create-user", sub)
        .button({icons: {
            primary: "ui-icon ui-icon-circle-plus"
        }})
        .click(function () {
            $("#dialog-form").dialog("open");
        });
    $("button.del_acc").button({
        icons: {
            primary: "ui-icon ui-icon-circle-minus"
        },
        text: false
    })
    $("button.mod_acc").button({
        icons: {
            primary: "ui-icon ui-icon-pencil"
        },
        text: false
    })
}

$(function () {
    var $div = $("div#DATA");
    botvote.data["html"] = $div.html();
    botvote.getPluginList();

    $div.remove();
})
