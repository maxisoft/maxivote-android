__author__ = 'maxisoft'
import gc

import cherrypy

from plugin import *


try:
	from plugin.BotVoteBase.DB import *
except ImportError:
	from BotVoteBase.DB import *

try:
	from plugin.BotVoteBase import BotBase
except ImportError:
	from BotVoteBase import BotBase

from utils.utils import crypt, read_iv
from theglobals import KEY_PRIVATE_FILE

try:
	from importhook.cphook import CherryPyHook
except ImportError:
	from cphook import CherryPyHook


class Root:

	def __init__(self):
		for klass in inheritors(CherryPyHook):
			instance = klass()
			setattr(self, instance.name, instance)

	@cherrypy.expose
	def index(self):
		raise cherrypy.HTTPRedirect("index.html")

	@cherrypy.expose
	def get_all_account(self, site):
		db = create_database('sqlite:%s' % 'DB.db')
		store = BotVoteStore(db)
		accs = store.get_all_accounts(site)
		accs = [acc for acc in accs]
		ret = json.dumps(accs, cls=ComplexEncoder)
		store.close()
		del store
		del db
		gc.collect()
		response = cherrypy.response
		response.headers['Content-Type'] = 'application/json'
		return ret

	@cherrypy.expose
	def get_all_vote(self, site):
		db = create_database('sqlite:%s' % 'DB.db')
		store = BotVoteStore(db)
		votes = store.get_all_vote(site)
		votes = [vote for vote in votes]
		ret = json.dumps(votes, cls=ComplexEncoder)
		store.close()
		del store
		del db
		gc.collect()
		response = cherrypy.response
		response.headers['Content-Type'] = 'application/json'
		return ret

	@cherrypy.expose
	def get_all_vote_plugins(self):
		ret = inheritors(BotBase)
		ret = [klass.sitename for klass in ret]
		ret.sort()  # sort by name
		response = cherrypy.response
		response.headers['Content-Type'] = 'application/json'
		return json.dumps(ret)

	@cherrypy.expose
	def add_account(self, sitename, login, password, proxy=None, prior=0):
		ret = None
		response = cherrypy.response
		response.headers['Content-Type'] = 'application/json'
		acc = Account()
		if proxy:
			acc.proxy = unicode(proxy)
		acc.priority = int(prior)
		acc.login = unicode(login)
		acc.password = unicode(crypt(password, read_iv(KEY_PRIVATE_FILE)))
		acc.sitename = unicode(sitename)

		db = create_database('sqlite:%s' % 'DB.db')
		store = BotVoteStore(db)
		try:
			store.add_account(acc)
		except Exception, e:
			ret = json.dumps({'err': str(e)}) #TODO error analys
		else:
			ret = json.dumps({'msg': 'ok'})
		finally:
			store.close()
			del store
			del db
			gc.collect()

		return ret

	@cherrypy.expose
	def delete_account(self, id):
		ret = None
		response = cherrypy.response
		response.headers['Content-Type'] = 'application/json'
		id = int(id)
		db = create_database('sqlite:%s' % 'DB.db')
		store = BotVoteStore(db)
		try:
			store.delete_account(id)
		except Exception, e:
			ret = json.dumps({'err': str(e)})
		else:
			ret = json.dumps({'msg': 'ok'})
		finally:
			store.close()
			del store
			del db
			gc.collect()

		return ret