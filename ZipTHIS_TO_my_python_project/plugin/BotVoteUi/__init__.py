__author__ = 'maxisoft'

import os
import shutil
import cherrypy

from plugin import *


try:
	from plugin.BotVoteBase.DB import *
except ImportError:
	from BotVoteBase.DB import *

try:
	from plugin.BotVoteBase import BotBase
except ImportError:
	from BotVoteBase import BotBase

import importhook
import logging

from site import Root


class BotVoteUi(PluginThread):
	def run(self):
		for handler in cherrypy.log.access_log.handlers:
			cherrypy.log.access_log.removeHandler(handler)
		cherrypy.log.error_log.setLevel(logging.WARN)
		cherrypy.config.update({
		'server.socket_host': '127.0.0.1',
		'server.socket_port': 9645,
		'environment': 'production',
		'log.error_file': os.path.join(LOG_DIR, 'site.log'),
		'log.access_file': "",
		'log.screen': False
		})
		current_dir = os.path.dirname(os.path.abspath(__file__))
		conf = {'/': {'tools.staticdir.on': True,
		              'tools.staticdir.dir': os.path.join(current_dir, 'html')
		}}
		cherrypy.quickstart(Root(), config=conf)