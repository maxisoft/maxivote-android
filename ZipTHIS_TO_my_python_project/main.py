# -*- coding: UTF-8 -*-
import android, time
import os, sys
droid = android.Android()

try:
	from theglobals import EVENTS, INIT_OBJ, PLUGINLIST, LOG_DIR, INSTALL_DIR, KEY_PRIVATE_FILE
	from Crypto import Random
	import sys
	#key create
	if not os.path.isfile(KEY_PRIVATE_FILE):
		with open(KEY_PRIVATE_FILE, 'wb') as f:
			f.write(Random.get_random_bytes(32 << 8))

	sys.path.append(INSTALL_DIR)
	import importhook
	EVENTS["init"] += INIT_OBJ
	EVENTS["init"] += PLUGINLIST
	EVENTS["init"]()
	droid.makeToast("MaxiVote loaded !")
	time.sleep(3)
except Exception, e:
	droid.makeToast("MaxiVote error : " + str(e))
	time.sleep(3)